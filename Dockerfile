FROM alpine:latest as build-stage

# Install prerequisites
RUN apk --no-cache add --update \
        gnupg \
        build-base \
        libevent \
        libevent-dev \
        libressl \
        libressl-dev \
        xz-libs \
        xz-dev \
        zlib \
        zlib-dev \
        zstd \
        zstd-dev \
      && wget -q https://dist.torproject.org/tor-0.4.5.6.tar.gz \
      && tar xf tor-0.4.5.6.tar.gz \
      && cd tor-0.4.5.6 \
      && ./configure \
      && make install \
      && ls -R /usr/local/

FROM alpine:latest
MAINTAINER Ablative Hosting <support@ablative.hosting>

#BSD habits die hard
ENV TOR_USER=_tor

# Installing dependencies of Tor and pwgen
RUN apk --no-cache add --update \
      libevent \
      libressl \
      xz-libs \
      zlib \
      zstd \
      zstd-dev \
      pwgen

# Copy Tor
COPY --from=build-stage /usr/local/ /usr/local/

# Create an unprivileged tor user
#RUN addgroup -g 19001 -S $TOR_USER && adduser -u 19001 -G $TOR_USER -S $TOR_USER
RUN addgroup -S $TOR_USER && adduser -G $TOR_USER -S $TOR_USER

# Copy Tor configuration file
COPY ./torrc /etc/tor/torrc

# Copy docker-entrypoint
COPY ./docker-entrypoint /usr/local/bin/

# Persist data
VOLUME /etc/tor /var/lib/tor

EXPOSE 9051 9050 

ENTRYPOINT ["docker-entrypoint"]
CMD ["tor", "-f", "/etc/tor/torrc"]
