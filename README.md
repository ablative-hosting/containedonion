# ContainedOnion
## A 'Public' SOCKS5 / ControlPort Tor Daemon

This Docker image presents a Tor SOCKS5 proxy on 9050 and a ControlPort on 
9051.

This is designed to operate in a local Kubernetes namespace or local dev env
such as docker-compose to aid in the development of [OnionWatch.email](https://onionwatch.email)
and [Cwtch.im](https://cwtch.im/)

## Usage
Pass `CONTROL_HASH` _(tor --hash-password foo)_ to set your own password for the
ControlPort otherwise a random one will be generated.
